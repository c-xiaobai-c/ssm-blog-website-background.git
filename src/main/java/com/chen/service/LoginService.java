package com.chen.service;

import com.chen.pojo.Administrator;

import java.util.Map;

public interface LoginService {


    Administrator queryAdministrator(Map<String,Object> map);//万能的Map

    int addFrequency(Map<String,Object> map);//添加登录次数
}
