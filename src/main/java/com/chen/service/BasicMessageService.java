package com.chen.service;

import com.chen.pojo.*;

import java.util.List;
import java.util.Map;

public interface BasicMessageService {
    List<BasicMessage> basicMessage();//查询网站的基本信息
    List<Article> article(int n);//查询全部用户发布的文章文章
    BasicMessage articleNumber();//查询全部用户发布未审核的文章的总数
    List<Article> queryArticle(String name);//通过昵称或标题查询文章
    int  audit(Map<String,Object> map);//审核文章
    Article previewArticle(int id);//查询单个用户发布的未审核的文章
    List<User> user(int n);//查询博客用户
    User userSum();//查询博客用户的总数
    List<User> queryUser(String name);//根据昵称或手机号查询博客用户
    List<AccessList> accessList(int n);//查询网站被访问列表
    AccessList accessListSum();//查询博客用户的总数
    List<DataAnalysis> queryDaySum();//查询一周中每天的访问量
    List<DataAnalysis> queryYearSum();//查询一年中每月的访问量
    List<User> findExcel();//导出用户表
    List<DataAnalysis> queryRegisterDaySum();//查询一周中每天的用户注册量
    List<DataAnalysis> queryRegisterYearSum();//查询一年中每月的用户注册量
    List<Administrator>  queryAdministrator();//查询管理员列表
    int addAdministrator(Administrator administrator);//添加管理员
    Administrator queryAdministrator2(String account);//防止添加管理员账号重复
    int deleteAdministrator(String account);//删除管理员账号
    int saveSiteArticles(SiteArticles articles);//添加站点文章
    List<SiteArticles> siteArticles(int n);//查询站点文章
    SiteArticles siteArticlesSum();//查询站点文章的总数
    int deleteSiteArticles(String id);//删除站点文章
    SiteArticles querySiteArticles(String id);//查询单篇站点文章
}
