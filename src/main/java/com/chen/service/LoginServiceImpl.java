package com.chen.service;

import com.chen.dao.LoginMapper;
import com.chen.pojo.Administrator;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class LoginServiceImpl implements LoginService{
    //调用dao层的操作，设置一个set接口，方便Spring管理
   private LoginMapper loginMapper;

    public void setLoginMapper(LoginMapper loginMapper) {
        this.loginMapper = loginMapper;
    }



    public Administrator queryAdministrator(Map<String, Object> map) {
        return loginMapper.queryAdministrator( map);
    }

    public int addFrequency(Map<String, Object> map) {
        return loginMapper.addFrequency(map);
    }
}
