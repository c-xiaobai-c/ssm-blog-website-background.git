package com.chen.service;

import com.chen.dao.BasicMessageMapper;
import com.chen.pojo.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public class BasicMessageServiceImpl implements BasicMessageService{




    private BasicMessageMapper basicMessageMapper;
    //调用dao层的操作，设置一个set接口，方便Spring管理
    public void setBasicMessageMapper(BasicMessageMapper basicMessageMapper) {
        this.basicMessageMapper=basicMessageMapper;
    }
    public List<BasicMessage> basicMessage() {

        return basicMessageMapper.basicMessage();//返回网站的基本信息
    }


    public List<Article> article(int n) {
        return basicMessageMapper.article(n);//返回所有的用户发布的未审核的文章
    }

    public BasicMessage articleNumber() {//返回所有的用户发布的未审核文章的总数
        return basicMessageMapper.articleNumber();
    }//返回所有的用户发布的未审核的文章的数量

    public List<Article> queryArticle(String name) {
        return basicMessageMapper.queryArticle(name);
    }


    public int audit(Map<String,Object> map) {//审核文章
        return  basicMessageMapper.audit(map);

    }

    public Article previewArticle(int id) {//查询单个用户发布的未审核的文章
        return basicMessageMapper.previewArticle(id) ;
    }

    public List<User> user(int n) {
        return basicMessageMapper.user(n);
    }

    public User userSum() {
        return basicMessageMapper.userSum();
    }

    public  List<User> queryUser(String name) {
        return basicMessageMapper.queryUser(name);
    }

    public List<AccessList> accessList(int n) {
        return basicMessageMapper.accessList(n);
    }

    public AccessList accessListSum() {
        return basicMessageMapper.accessListSum();
    }

    public  List<DataAnalysis> queryDaySum() {
        return basicMessageMapper.queryDaySum();
    }

    public List<DataAnalysis> queryYearSum() {
        return basicMessageMapper.queryYearSum();
    }

    public List<User> findExcel() {
        return basicMessageMapper.findExcel();
    }

    public List<DataAnalysis> queryRegisterDaySum() {
        return basicMessageMapper.queryRegisterDaySum();
    }

    public List<DataAnalysis> queryRegisterYearSum() {
        return basicMessageMapper.queryRegisterYearSum();
    }

    public List<Administrator> queryAdministrator() {
        return basicMessageMapper.queryAdministrator();
    }

    public int addAdministrator(Administrator administrator) {
        return basicMessageMapper.addAdministrator(administrator);
    }

    public Administrator queryAdministrator2(String name) {
        return basicMessageMapper.queryAdministrator2(name);
    }

    public int deleteAdministrator(String account) {
        return basicMessageMapper.deleteAdministrator(account);
    }

    public int saveSiteArticles(SiteArticles articles) {
        return basicMessageMapper.saveSiteArticles(articles);
    }

    public List<SiteArticles> siteArticles(int n) {
        return basicMessageMapper.siteArticles(n);
    }

    public SiteArticles siteArticlesSum() {
        return basicMessageMapper.siteArticlesSum();
    }

    public int deleteSiteArticles(String id) {
        return basicMessageMapper.deleteSiteArticles(id);
    }

    public SiteArticles querySiteArticles(String id) {
        return basicMessageMapper.querySiteArticles(id);
    }


}
