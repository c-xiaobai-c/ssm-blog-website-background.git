package com.chen.controller;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.fastjson.JSON;
import com.chen.pojo.*;
import com.chen.service.BasicMessageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
*
*
*
*/

@Controller
public class MainController {

    @Resource(name="BasicMessageServiceImpl")
    private BasicMessageService basicMessageService;

    //跳转到博文管理页面
    @RequestMapping("/design")
    public String design(Model model) throws Exception {
        List<Article> articles=basicMessageService.article(0);
        model.addAttribute("articles",articles);
        model.addAttribute("articleNumber",basicMessageService.articleNumber());
        return "design";
    }
    //更新博文管理页面的文章的数量
    @RequestMapping("/ArticlePage")
    @ResponseBody
    public String ArticlePage(String n,Model model) throws Exception {
        List<Article> articles=basicMessageService.article(10*(Integer.parseInt(n)-1));
        String str1 = JSON.toJSONString(articles);//转换为json字符串
        return str1;
    }


    //根据昵称或文章标题查询文章
    @RequestMapping("/queryArticle")
    @ResponseBody
    public String queryArticle(String name,Model model) throws Exception {
        List<Article> articles=basicMessageService.queryArticle(name);
        String str1 = JSON.toJSONString(articles);//转换为json字符串
        return str1;
    }


    /*
    *
    * 预览未审核的文章
    *
    *
    * */
    //@RequestMapping("previewArticle")
    @GetMapping("/previewArticle/{id}")
    public String previewArticle(@PathVariable int id, Model model){
     Article article=basicMessageService.previewArticle(id);
     model.addAttribute("previewArticle",article);
     return "previewArticle";
    }


 /*
 *
 *对文章进行审核
  *
  */
    @RequestMapping("audit")
    @ResponseBody
    public String  audit(String id,String status){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
       if(status.equals("1")){
           map.put("status", "通过审核");
       }
        if(status.equals("0")){
            map.put("status","不通过审核");
        }
        int i=basicMessageService.audit(map);
        if(i>0){
            return "操作成功";
        }
        else {
            return "操作失败";
        }

    }
 /*
 *
 *  跳转到用户列表页面
 *
 *
 *
 * */
    @RequestMapping("/userList")
    public String userList(Model model) throws Exception {
        List<User> users=basicMessageService.user(0);
        model.addAttribute("users",users);
        User userSum=basicMessageService.userSum();
        model.addAttribute("userSum",userSum);
        return "userList";
    }

    /*
     *
     *  更新用户列表，进行翻页
     *
     *
     *
     * */
    @RequestMapping("/updateUserList")
    @ResponseBody
    public String updateUserList(String n,Model model) throws Exception {
        List<User> users=basicMessageService.user(5*(Integer.parseInt(n)-1));//转换为json字符串
        String str1 = JSON.toJSONString(users);
        model.addAttribute("users",users);
        return str1;
    }

    /*
     *
     *  查询用户列表的用户
     *
     *
     *
     * */
    @RequestMapping("/queryUser")
    @ResponseBody
    public String queryUser(String name,Model model) throws Exception {

        List<User> users=basicMessageService.queryUser(name);//转换为json字符串
        String str1 = JSON.toJSONString(users);
        model.addAttribute("queryUser",users);
        return str1;


    }
/*
*
* 导出用户信息为Excel
*
* */
    @RequestMapping("/exportExcel")
    @ResponseBody
    public void export(HttpServletResponse response) throws IOException {
        List<User> list = basicMessageService.findExcel();
       // System.out.println("list========="+list);
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "用户信息表";
        Sheet sheet = new Sheet(1, 0,User.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("用户信息");
        writer.write(list, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
        out.close();
        return ;
    }


/*
*
* 访问列表
*
* */
    @RequestMapping("/accessList")
    public String accessList(Model model) throws Exception {

        List<AccessList> accessLists=basicMessageService.accessList(0);
        model.addAttribute("accessLists",accessLists);
        AccessList accessList=basicMessageService.accessListSum();
        model.addAttribute("accessList",accessList);
        return "accessList";
    }

    /*
     *
     * 访问列表翻页
     *
     * */
    @RequestMapping("/updateAccessList")
    @ResponseBody
    public String updateAccessList(String n,Model model) throws Exception {
        List<AccessList> accessLists=basicMessageService.accessList(10*(Integer.parseInt(n)-1));
        String str1 = JSON.toJSONString(accessLists);

        return str1;
    }


    //跳转到数据分析页面
    @RequestMapping("/websiteDataAnalysis")
    public String test() throws Exception {
        return "websiteDataAnalysis";
    }


    //跳转到折线图
    @RequestMapping("/lineChat")
    public String  lineChat(Model model) throws Exception {
        List<DataAnalysis> dataAnalyses=basicMessageService.queryDaySum();
        model.addAttribute("daySum",dataAnalyses);
        List<DataAnalysis> dataAnalyses2=basicMessageService.queryYearSum();
        model.addAttribute("YearSum",dataAnalyses2);
        return "lineChat";
    }

   /*
   *
   *
   * 查询用户注册数量的折线图
   *
   *
   * */
    @RequestMapping("/registerLineChat")
    @ResponseBody
    public String  registerLineChat(Model model) throws Exception {
        List<DataAnalysis> dataAnalyses=basicMessageService.queryRegisterDaySum();//查询周用户注册量
        List<DataAnalysis> dataAnalyses2=basicMessageService.queryRegisterYearSum();//查询年用户注册量
        for(DataAnalysis dataAnalysis:dataAnalyses2) {
            dataAnalyses.add(dataAnalysis);
        }
        String str1 = JSON.toJSONString(dataAnalyses);
       // System.out.println("str1==========="+str1);
        model.addAttribute("test","test");
        return str1;
    }


    //跳转到管理员列表
    @RequestMapping("/AdministratorList")
    public String   AdministratorList(Model model) throws Exception {
        List<Administrator> administrators=basicMessageService.queryAdministrator();
        model.addAttribute("administrators",administrators);
        return "AdministratorList";
    }

    //跳转到新增管理员
    @RequestMapping("/AddAdministrator")
    public String   AddAdministrator(HttpSession session,Model model) throws Exception {
        Administrator administrator= (Administrator) session.getAttribute("user");
        if(administrator.getLevel().equals("超级管理员")){
            return "AddAdministrator";
        }
        return "error";
    }
    //跳转到新增管理员
    @RequestMapping("/error")
    public String  error() throws Exception {

        return "error";
    }
    //添加管理员
    @RequestMapping("/add")
    @ResponseBody
    public String add(String name,String account,String password,String password2) throws Exception {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(account);
        //八位数字与字母组合
        String regStr="^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";
        if(name!=null&&account!=null&&password!=null&&password2!=null) {
            if (!m.matches()) {
                System.out.println("yes");
                return "请输入正确的手机号";
            } else if (!password.matches(regStr)) {
                System.out.println("yes2");
                return "密码长度要大于等于8（数字和英文）";
            } else if (!password.equals(password2)) {
                return "两次密码不一样，请重新输入";
            } else {
                if (basicMessageService.queryAdministrator2(account)==null) {
                    Administrator administrator = new Administrator(0, name, account, password, null, null, 0, null);
                    int n = basicMessageService.addAdministrator(administrator);
                    if (n > 0) {
                        return "添加成功";
                    } else {
                        return "添加失败";
                    }


                }
                else {
                    return "该账号已存在";
                }
            }
        }
            else {
                return "不能为空";
            }

    }

    //跳转到新增管理员
    @RequestMapping("/deleteAdministrator")
    @ResponseBody
    public String  deleteAdministrator(String account,HttpSession session) throws Exception {
        Administrator administrator= (Administrator) session.getAttribute("user");
        if(administrator.getLevel().equals("超级管理员")) {
            if (!administrator.getAccount().equals(account)) {
                int n = basicMessageService.deleteAdministrator(account);
                if (n > 0) {
                    return "删除成功";
                } else {
                    return "删除失败";
                }
            }
            else {
                return "不能删除超级管理员";
            }
        }
      else {
            return "你不是超级管理员，不能进行该操作";
        }


    }

    /*
    *
    * 进入站点管理页面
    * */
    @RequestMapping("/siteArticles")
    public String siteArticles(Model model){
        List<SiteArticles> siteArticles=basicMessageService.siteArticles(0);
        model.addAttribute("articles",siteArticles);
        model.addAttribute("articleNumber",basicMessageService.siteArticlesSum());
        return "siteArticles";
    }



    @RequestMapping("/siteArticlesPage")
    @ResponseBody
    public String siteArticlesPage(String n){
        List<SiteArticles>siteArticles=basicMessageService.siteArticles(10*(Integer.parseInt(n)-1));//转换为json字符串
        String str1 = JSON.toJSONString(siteArticles);
        return str1;
    }

    /*

    * 删除站点文章
    *
    * */
    @RequestMapping("/deleteSiteArticles")
    @ResponseBody
    public String  deleteSiteArticles(String id,HttpSession session) throws Exception {
        Administrator administrator= (Administrator)session.getAttribute("user");
        if(administrator.getLevel().equals("超级管理员")) {
                int n = basicMessageService.deleteSiteArticles(id);
                if (n > 0) {
                    return "删除成功";
                } else {
                    return "删除失败";
                }
        }
        else {
            return "你不是超级管理员，不能进行该操作";
        }
    }

    /*
    *
    * 预览站点文章
    *
    * */
    @GetMapping("/previewSiteArticles/{id}")
    public String previewSiteArticles(@PathVariable String id, Model model){
        SiteArticles article=basicMessageService.querySiteArticles(id);
        model.addAttribute("previewSiteArticles",article);
        return "previewSiteArticles";
    }




    /*
    *
    * 进入新增作品页面
    *
    * */
    @RequestMapping("/insert")
    public String insert(){

        return "insert";
    }


    @RequestMapping("/saveSiteArticles")
    @ResponseBody
    public String saveSiteArticles(SiteArticles siteArticles){
        System.out.println(siteArticles);
        if(siteArticles.getName()!=null&&siteArticles.getContent()!=null&&siteArticles.getTitle()!=null)
        {
            if(siteArticles.getPicture()!=null) {
                String dataPrix = "";
                String data = "";

                String[] d = siteArticles.getPicture().split("base64,");
                    if (d != null && d.length == 2) {
                        dataPrix = d[0];
                        data = d[1];
                    } else {
                        return "上传失败，图片数据不合法";
                     //   throw new Exception("上传失败，数据不合法");
                    }

                String suffix = "";
                if ("data:image/jpeg;".equalsIgnoreCase(dataPrix)) {//base64编码的jpeg图片数据
                    suffix = ".jpg";
                } else if ("data:image/x-icon;".equalsIgnoreCase(dataPrix)) {//base64编码的icon图片数据
                    suffix = ".ico";
                } else if ("data:image/gif;".equalsIgnoreCase(dataPrix)) {//base64编码的gif图片数据
                    suffix = ".gif";
                } else if ("data:image/png;".equalsIgnoreCase(dataPrix)) {//base64编码的png图片数据
                    suffix = ".png";
                } else {
                    return "上传失败，图片数据不合法";
                   // throw new Exception("上传图片格式不合法");
                }
            }
            int n=basicMessageService.saveSiteArticles(siteArticles);
             if(n>0){
                 return "success";
             }
            else{
                 return "上传失败";
             }
        }
        else{
            return "不能为空";
        }


    }



    //注销退出
    @RequestMapping("/cancellation")
    public String cancellation(HttpSession session) throws Exception {
        session.invalidate();//移除session会话
        return "login";
    }
}
