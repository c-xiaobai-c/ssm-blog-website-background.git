package com.chen.controller;

import com.chen.pojo.Administrator;
import com.chen.pojo.BasicMessage;
import com.chen.service.BasicMessageService;
import com.chen.service.LoginService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
public class LoginController {

    @Resource(name="LoginServiceImpl")
    private LoginService loginService;
    @Resource(name="BasicMessageServiceImpl")
    private BasicMessageService basicMessageService;

    //跳转到登陆页面
    @RequestMapping("/jumplogin")
    public String jumpLogin() throws Exception {
        return "main";
    }

    //跳转到成功页面
    @RequestMapping("/jumpSuccess")
    public String jumpSuccess(Model model) throws Exception {
        List<BasicMessage> list = basicMessageService.basicMessage();
        BasicMessage basicMessage=new BasicMessage(list.get(0).getAdministratorNumber(),list.get(1).getAdministratorNumber(),list.get(2).getAdministratorNumber(),list.get(3).getAdministratorNumber());
        System.out.println(basicMessage);
        model.addAttribute("list",basicMessage);
        model.addAttribute("time",new Date());
        return "main";
    }

    //获取登录验证码
    @RequestMapping("/getcode")
    @ResponseBody
    public String getcode(HttpSession session) throws Exception {
        // 向session记录验证码
        String verifyCode = String.valueOf(new Random().nextInt(89999) + 10000);//生成5位验证码
        session.setAttribute("verifyCode",verifyCode);
            return verifyCode;
    }

    //登陆提交
    @RequestMapping("/login")
    @ResponseBody
    public String login(HttpSession session, String account,String password,String code) throws Exception {
        // 向session记录用户身份信息

      if(session.getAttribute("verifyCode")!=null) {
          if(session.getAttribute("verifyCode").equals(code)) {
              session.removeAttribute("verifyCode");
              Map<String, Object> map = new HashMap<String, Object>();
              map.put("account", account);
              map.put("password", password);
              Administrator administrator2 = loginService.queryAdministrator(map);
              session.setAttribute("user", administrator2);
              if (account.equals("") || password.equals("")) {
                  System.out.println("=========");
                  return "账号不能为空或密码不能为空";
              } else if (administrator2 != null) {

                      int n = loginService.addFrequency(map);  //添加登录次数，以及登录时间
                      System.out.println("login===========" + n);
                  return "成功";

              } else {
                  return "失败";

              }
          }
          else{
              session.removeAttribute("verifyCode");
              return "验证码错误";
          }
      }
        else {
            return "错误";

        }
    }


    //退出登陆
    @RequestMapping("logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.invalidate();
        return "login";
    }


}
