package com.chen.dao;

import com.chen.pojo.Administrator;

import java.util.Map;

public interface LoginMapper {



    Administrator queryAdministrator(Map<String,Object> map);//万能的Map，用户登录验证

    int addFrequency(Map<String,Object> map);//添加登录次数


}
