package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class SiteArticles {

  private  int id;
  private String title;
  private String name;
  private String picture;
  private String content;
  private String time;
  private int    pageview;
  private int    articlesSum;


}
