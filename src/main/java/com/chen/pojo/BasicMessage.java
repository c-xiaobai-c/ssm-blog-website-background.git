package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicMessage {
    private int administratorNumber;//网站管理员数量
    private int userNumber;//网站用户数量
    private int websiteNumber;//网站访问数量
    private int articleNumber;//网站文章数量


}
