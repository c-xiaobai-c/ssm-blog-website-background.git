package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {

    private int id;
    private String name;//用户昵称
    private String phonenumber;//用户手机号
    private String title;//文章标题
    private String content;//文章内容
    private String  articlelabel;//文章标签
    private String subfield;//分栏专类
    private String articletype;//文章类型
    private String modality;//发布形式
    private String time;//发布时间
    private int pageview;//浏览量
    private int  likenumber;//点赞数量
    private int comment;//评论数量
    private int collect;//收藏数量
    private String status;//状态

}
