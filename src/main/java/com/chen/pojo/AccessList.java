package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
访问列表
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessList {
    private int id;
    private String time;
    private String address;
    private String accessListSum;
}
