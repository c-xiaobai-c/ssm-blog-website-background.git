package com.chen.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//该继承（BaseRowModel）方法用于生成Excel表信息
public class User extends BaseRowModel {
    //value为列名,index为第几列（用于生成Excel表信息）
    @ExcelProperty(value = "昵称",index = 0 )
    private String name;

    private String password;
    @ExcelProperty(value = "手机号",index = 1)
    private String phonenumber;
    @ExcelProperty(value = "邮箱",index = 2)
    private String mailbox;

    private String picture;
    @ExcelProperty(value = "用户等级",index = 4)
    private String level;
    @ExcelProperty(value = "登录时间",index = 5)
    private String logintime;
    @ExcelProperty(value = "退出时间",index = 6)
    private String outtime;
    @ExcelProperty(value = "注册时间",index = 7)
    private String registertime;
    @ExcelProperty(value = "登录频次",index = 8)
    private String frequency;
    private String userSum;//用户总数
}
