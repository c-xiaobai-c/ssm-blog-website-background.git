package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Administrator {
    private int id;
    private String name;
    private String account;
    private String password;
    private String logintime;
    private String outtime;
    private int frequency;
    private String level;
}
