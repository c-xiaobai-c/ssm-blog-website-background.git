package com.chen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataAnalysis {
//    private int sundaySum;//最近周天的访问量
//    private int mondaySum;//最近周一的访问量
//    private int tuesdaySum;//最近周二的访问量
//    private int wednesdaySum;//最近周三的访问量
//    private int thursdaySum;//最近周四的访问量
//    private int fridaySum;//最近周五的访问量
//    private int saturday;//最近周六的访问量
    private String day;//天
    private String time;
    private int daySum;//一周中每天的访问量
    private String month;//月份
    private int    monthSum;//每个月的访问量


}
