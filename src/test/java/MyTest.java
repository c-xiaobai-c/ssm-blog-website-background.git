import com.chen.pojo.*;
import com.chen.service.BasicMessageService;
import com.chen.service.LoginService;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyTest {

    //登录测试，通过账号密码是否能查询到数据库的账号密码
     @Test
    public void test(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        LoginService loginService= (LoginService) context.getBean("LoginServiceImpl");
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("account","123");
        map.put("password","123");
        Administrator administrator=loginService.queryAdministrator(map);
        loginService.addFrequency(map);
        System.out.println(administrator);

    }

 /*
 *
 * 测试查询用户的文章
 *
    */
    @Test
    public void test2(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<Article> basicMessage=basicMessageService.article(1);
        System.out.println(basicMessage);
        BasicMessage basicMessage1=basicMessageService.articleNumber();
        System.out.println(basicMessage1);

    }

    /*
     *
     * 测试查询单个用户的文章
     *
     */
    @Test
    public void test3(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        Article article=basicMessageService.previewArticle(35);
        System.out.println(article);


    }

    /*
     *
     * 测试查询博客用户
     *
     */
    @Test
    public void test4(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<User> user=basicMessageService.user(1);
        System.out.println(user);


    }
    /*
     *
     * 测试查询博客用户的总数
     *
     */
    @Test
    public void test5(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
         User user=basicMessageService.userSum();
        System.out.println(user);


    }

    /*
     *
     * 测试根据用户昵称或手机号查询博客用户
     *
     */
    @Test
    public void test6(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<User> user=basicMessageService.queryUser("小");
        System.out.println(user);


    }

    /*
     *
     * 测试根据昵称或标题查询文章
     *
     */
    @Test
    public void test7(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
       List<Article> articles=basicMessageService.queryArticle("测试");
        System.out.println(articles);


    }
    /*
     *
     * 测试网站被访问列表
     *
     */
    @Test
    public void test8(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
       List<AccessList> accessLists=basicMessageService.accessList(1);
        System.out.println(accessLists);
        AccessList accessListSum=basicMessageService.accessListSum();
        System.out.println(accessListSum);

    }
    /*
     *
     * 查询一周中每天的访问量
     *
     */
    @Test
    public void test9(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<DataAnalysis> dataAnalyses=basicMessageService.queryDaySum();
        System.out.println(dataAnalyses);


    }
    /*
     *
     * 查询一年中每月的访问量
     *
     */
    @Test
    public void test10(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<DataAnalysis> dataAnalyses=basicMessageService.queryYearSum();
        System.out.println(dataAnalyses);


    }
    /*
     *
     * 查询所有用户
     *
     */
    @Test
    public void test11(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<User> users=basicMessageService.findExcel();
        System.out.println(users);


    }


    /*
     *
     * 查询一周中每天的注册量
     *
     */
    @Test
    public void test12(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<DataAnalysis> dataAnalyses=basicMessageService.queryRegisterDaySum();
        System.out.println(dataAnalyses);


    }
    /*
     *
     * 查询一年中每月的注册量
     *
     */
    @Test
    public void test13(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<DataAnalysis> dataAnalyses=basicMessageService.queryRegisterYearSum();
        System.out.println(dataAnalyses);


    }

    /*
     *
     * 查询管理员列表
     *
     */
    @Test
    public void test14(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<Administrator> administrators=basicMessageService.queryAdministrator();
        System.out.println(administrators);


    }
    @Test
    public void test15(){

            String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";
            Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher("15119747300");
           if(m.matches()) {
               System.out.println("yes");

        }
           else {
               System.out.println("no");

           }
        String input="1234567c";
       // String regStr = "^([A-Z]|[a-z]|[0-9]|[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*()――+|{}【】‘；：”“'。，、？]){6,20}$";
      //八位数字与字母组合
        String regStr="^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]+$";
      if(input.matches(regStr)){
          System.out.println("yes2");
      }
      else {
          System.out.println("no2");

      }
    }
    /*
     *
     * 查询管理员列表
     *
     */
    @Test
    public void test16(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        Administrator administrator=new Administrator(0,"陈锦贤2","15119747306","12345678a",null,null,0,null);
        int n=basicMessageService.addAdministrator(administrator);
        System.out.println(n);


    }

    /*
     *
     * 防止添加管理员账号重复
     *
     */
    @Test
    public void test17(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        Administrator administrator=basicMessageService.queryAdministrator2("15119747306");
        System.out.println(administrator);


    }
    /*
     *
     * 删除管理员账号
     *
     */
    @Test
    public void test18(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
     int n=basicMessageService.deleteAdministrator("15119747307");
        System.out.println(n);


    }

    /*
     *
     * 添加站点文章
     *
     */
    @Test
    public void test19(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        int n=basicMessageService.saveSiteArticles(new SiteArticles(0,"测试","晓晓",null,"哈哈哈哈哈哈",null,0,0));
        System.out.println(n);


    }


    /*
     *
     * 查询站点文章
     *
     */
    @Test
    public void test20(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        List<SiteArticles> siteArticles=basicMessageService.siteArticles(0);
       // System.out.println(siteArticles);


    }
    /*
     *
     * 查询站点文数量
     *
     */
    @Test
    public void test21(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        SiteArticles siteArticles=basicMessageService.siteArticlesSum();
        System.out.println(siteArticles);


    }
    /*
     *
     * 删除站点文章
     *
     */
    @Test
    public void test22(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
        int n=basicMessageService.deleteSiteArticles("1");
        System.out.println(n);


    }

    /*
     *
     * 查询单篇站点文章
     *
     */
    @Test
    public void test23(){

        ApplicationContext context= new ClassPathXmlApplicationContext("applicationContext.xml");
        BasicMessageService basicMessageService= (BasicMessageService) context.getBean("BasicMessageServiceImpl");
       SiteArticles siteArticles=basicMessageService.querySiteArticles("2");
        System.out.println(siteArticles);


    }

}


