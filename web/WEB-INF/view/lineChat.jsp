<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/3
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>折线图分析</title>
    <!-- 引入 echarts.js -->
    <script src="https://cdn.staticfile.org/echarts/4.3.0/echarts.min.js"></script>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%--<div id="main" style="width: 600px;height:400px;"></div>--%>

<div class="btn-group" style="padding-top: 10px">
    <button type="button" class="btn btn-default dropdown-toggle"
            data-toggle="dropdown">
        选择分析<span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li id="website"><a href="#">分析网站访问量</a></li>
        <li id="register"><a href="#">分析用户注册</a></li>
       <%-- <li id="login"><a href="#">分析用户登录</a></li>--%>
    </ul>
</div>
​
<form style="padding-top: 70px">
    <span id="main" style="width: 700px;height:400px;float: left"></span>
    <span id="main2" style="width: 700px;height:400px;float: right"></span>
    <span id="main3" style="width: 700px;height:400px;float: left;display: none;"></span>
    <span id="main4" style="width: 700px;height:400px;float: right;display: none;"></span>
</form>
<script>
    var SundaySum = 0, MondaySum = 0, TuesdaySum = 0, WednesdaySum = 0, ThursdaySum = 0, FridaySum = 0, SaturdaySum = 0;
    var  sum=parseInt(SundaySum)+parseInt(MondaySum)+parseInt(TuesdaySum)+parseInt(WednesdaySum)+parseInt(ThursdaySum)+parseInt(FridaySum)+parseInt(SaturdaySum);

    <c:forEach var="daySum" items="${requestScope.get('daySum')}">
    <c:if test="${daySum.getDay()=='6'}">
    SundaySum =${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='0'}">
    MondaySum =${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='1'}">
   TuesdaySum =${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='2'}">
    WednesdaySum =${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='3'}">
   ThursdaySum=${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='4'}">
   FridaySum =${daySum.getDaySum()};
    </c:if>
    <c:if test="${daySum.getDay()=='5'}">
   SaturdaySum =${daySum.getDaySum()};
    </c:if>
    </c:forEach>


    var JanuarySum=0, FebruarySum=0, MarchSum=0, AprilSum=0, MaySum=0, JuneSum=0, JulySum=0, AugustSum=0, SeptemberSum=0, OctoberSum=0, NovemberSum=0, DecemberSUm=0;
    <c:forEach var="YearSum" items="${requestScope.get('YearSum')}">
    <c:if test="${YearSum.getMonth()=='1'}">
    JanuarySum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='2'}">
    FebruarySum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='3'}">
    MarchSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='4'}">
    AprilSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='5'}">
    MaySum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='6'}">
   JuneSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='7'}">
    JulySum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='8'}">
    AugustSum=${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='9'}">
    SeptemberSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='10'}">
    OctoberSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='11'}">
    NovemberSum =${YearSum.getMonthSum()};
    </c:if>
    <c:if test="${YearSum.getMonth()=='12'}">
    DecemberSUm=${YearSum.getMonthSum()};
    </c:if>
    </c:forEach>

    var chartDom = document.getElementById('main');
    var myChart = echarts.init(chartDom);
    var option;

    var chartDom2 = document.getElementById('main2');
    var myChart2 = echarts.init(chartDom2);


    //var  sum=parseInt(SundaySum)+parseInt(MondaySum)+parseInt(TuesdaySum)+parseInt(WednesdaySum)+parseInt(ThursdaySum)+parseInt(FridaySum)+parseInt(SaturdaySum);

    option = {
        title: {
            text: '最近一周数据分析',
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['网站访问量']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['周日','周一', '周二', '周三', '周四', '周五', '周六']
        },
        yAxis: {
            type: 'value'
        },
        series: [


            {
                name: '网站访问量',
                type: 'line',
                stack: '总量',
                data: [SundaySum, MondaySum, TuesdaySum,WednesdaySum, ThursdaySum,FridaySum, SaturdaySum]
            }
        ]
    };
    option2 = {
        title: {
            text: '最近一年数据分析',
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['网站访问量']
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['一月','二月','三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
        },
        yAxis: {
            type: 'value'
        },
        series: [


            {
                name: '网站访问量',
                type: 'line',
                stack: '总量',
                data: [JanuarySum, FebruarySum, MarchSum, AprilSum, MaySum, JuneSum, JulySum, AugustSum, SeptemberSum, OctoberSum, NovemberSum, DecemberSUm]
            }
        ]
    };

    option && myChart.setOption(option);

    option2 && myChart2.setOption(option2);

    var SundaySum2 = 0, MondaySum2 = 0, TuesdaySum2 = 0, WednesdaySum2 = 0, ThursdaySum2 = 0, FridaySum2 = 0, SaturdaySum2 = 0;
    var JanuarySum2=0, FebruarySum2=0, MarchSum2=0, AprilSum2=0, MaySum2=0, JuneSum2=0, JulySum2=0, AugustSum2=0, SeptemberSum2=0, OctoberSum2=0, NovemberSum2=0, DecemberSUm2=0;


    $("#register").click(function(){

       // var SundaySum2 = 0, MondaySum2 = 0, TuesdaySum2 = 0, WednesdaySum2 = 0, ThursdaySum2 = 0, FridaySum2 = 0, SaturdaySum2 = 0;
       // var JanuarySum2=0, FebruarySum2=0, MarchSum2=0, AprilSum2=0, MaySum2=0, JuneSum2=0, JulySum2=0, AugustSum2=0, SeptemberSum2=0, OctoberSum2=0, NovemberSum2=0, DecemberSUm2=0;
        $.post("${pageContext.request.contextPath}/registerLineChat",
            {

            },
            function(data){
                var json = JSON.parse(data);

                for (i2 in json) {
                    if(json[i2].day=='6'){
                        SundaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='0'){
                        MondaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='1'){
                       TuesdaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='2'){
                        WednesdaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='3'){
                        ThursdaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='4'){
                        FridaySum2=json[i2].daySum;
                    }
                    if(json[i2].day=='5'){
                       SaturdaySum2=json[i2].daySum;

                    }

                    if(json[i2].month=='1'){
                       JanuarySum2=json[i2].monthSum;
                    }
                    if(json[i2].month=='2'){
                        FebruarySum2=json[i2].monthSum;
                    }
                    if(json[i2].month=='3'){
                        MarchSum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='4'){
                        AprilSum2=json[i2].monthSum;
                    }
                    if(json[i2].month=='5'){
                        MaySum2=json[i2].monthSum;
                    }
                    if(json[i2].month=='6'){
                        JuneSum2=json[i2].monthSum;
                    }
                    if(json[i2].month=='7'){
                        JulySum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='8'){
                        AugustSum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='9'){
                       SeptemberSum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='10'){
                        OctoberSum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='11'){
                        NovemberSum2=json[i2].monthSum;

                    }
                    if(json[i2].month=='12'){
                       DecemberSUm2=json[i2].monthSum;

                    }



                }
                register();
            });
    });
  /*  $("#register").click(function(){

    });*/



    function register(){

        $("#main").hide();
        $("#main2").hide();
        $("#main3").show();
        $("#main4").show();
        var chartDom3 = document.getElementById('main3');
        var myChart3 = echarts.init(chartDom3);
        var chartDom4 = document.getElementById('main4');
        var myChart4 = echarts.init(chartDom4);
        option3 = {
            title: {
                text: '最近一周数据分析',
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['用户注册量']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['周日','周一', '周二', '周三', '周四', '周五', '周六']
            },
            yAxis: {
                type: 'value'
            },
            series: [


                {
                    name: '用户注册量',
                    type: 'line',
                    stack: '总量',
                    data: [SundaySum2, MondaySum2, TuesdaySum2,WednesdaySum2, ThursdaySum2,FridaySum2, SaturdaySum2]
                }
            ]
        };
        option4 = {
            title: {
                text: '最近一年数据分析',
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['用户注册量']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['一月','二月','三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
            },
            yAxis: {
                type: 'value'
            },
            series: [


                {
                    name: '用户注册量',
                    type: 'line',
                    stack: '总量',
                    data: [JanuarySum2, FebruarySum2, MarchSum2, AprilSum2, MaySum2, JuneSum2, JulySum2, AugustSum2, SeptemberSum2, OctoberSum2, NovemberSum2, DecemberSUm2]
                }
            ]
        };

        option3 && myChart3.setOption(option3);
        option4 && myChart4.setOption(option4);
    }
    $("#website").click(function(){
        $("#main3").hide();
        $("#main4").hide();
        $("#main").show();
        $("#main2").show();
    });
    
    
</script>
</body>
</html>