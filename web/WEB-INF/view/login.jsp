<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/5/17
  Time: 16:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>博客后台</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/style.css">
<%--
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
--%>
   <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>

    <script>
        function login(){

            $.post("${pageContext.request.contextPath}/login",
                {
                    account:$("#account").val(),
                    password:$("#password").val(),
                    code:$("#code").val()
                },
                function(data,status){

                    if(data=="成功") {
                        window.location.assign("${pageContext.request.contextPath}/jumpSuccess")
                    }
                    else{
                        alert(data);
                    }
                });
        }
        function getcode(){

            $.post("${pageContext.request.contextPath}/getcode",
                {

                },
                function(data,status){
                    $("#getcode").val(data);
                });
        }
    </script>
</head>
<body>
<div class="zxcf_nav_wper">
    <div class="zxcf_nav clearfix px1000">
        <div class="zxcf_nav_l fl"><img src="..${pageContext.request.contextPath}/statics/images/zxcf_logo.png" alt=""></div>
        <div class="zxcf_nav_r fr">
            <img src="..${pageContext.request.contextPath}/statics/images/lg_pic01.png" alt="">


        </div>
    </div>
</div>
<!-- end  -->
<div class="login_con_wper">
    <div class="login_con px1000 ">
        <div class="lg_section clearfix">
            <div class="lg_section_l fl">
                <img src="..${pageContext.request.contextPath}/statics/images/lg_bg02.png">
            </div>
            <!-- end l -->
            <div class="lg_section_r fl">
                <h2 class="lg_sec_tit clearfix">
                    <span class="fl">登录</span>
                </h2>
              <%--  <form >--%>
                        <p class="mt20">
                            <input type="text"   placeholder="用户名/手机" class="lg_input01 lg_input" name="account" id="account" >
                        </p>
                        <p class="mt20">
                            <input type="password" placeholder="密码" class="lg_input02 lg_input" name="password" id="password" >
                        </p>
                    <p class="mt20">
                        <input type="submit" id="getcode" style="height:30px;width: 260px;" onclick="getcode()" value="点击生成验证码">

                    </p>
                <p class="mt20">
                    <input type="text" id="code" style="height:30px;width: 260px;" placeholder="填写验证码">

                </p>
<%--
                        <p class="clearfix lg_check"><span class="fl"><input type="checkbox">记住用户名</span><a href="#" class="fr">忘记密码？找回</a></p>
--%>
                  <%--      <p><a href="${pageContext.request.contextPath}/login" class="lg_btn">立即登陆</a></p>--%>
                    </br>
                    </fieldset>
               <%-- </form>--%>
                <button type="submit" onclick="login()"><a  class="lg_btn">立即登陆</a></button>
            </div>
        </div>
    </div>
</div>
<div class="zscf_bottom_wper">
    <div class="zscf_bottom px1000 clearfix">
       <%-- <p class="fl">© 2020 </p>
        <p class="fr">--%>

        </p>
    </div>
</div>




</body>
</html>