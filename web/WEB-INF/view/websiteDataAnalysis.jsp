<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/3
  Time: 0:23
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
    <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="index.html">首页</a></li>
                <li><a href="/" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a >管理员：${sessionScope.get('user').name}，欢迎使用！</a></li>
              <%--  <li><a href="#">修改密码</a></li>--%>
                <li><a href="${pageContext.request.contextPath}/cancellation">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/userList"><i class="icon-font">&#xe006;</i>用户列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="${pageContext.request.contextPath}/jumpSuccess">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">数据分析</span></div>
        </div>

        <div class="result-wrap">
            <form name="myform" id="myform" method="post">

                <div class="result-content" id="table">
                    <div > <iframe id="zi" frameborder="0" src="lineChat" width="100%"  height="700px" ></iframe>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <!--/main-->
</div>

</body>
</html>