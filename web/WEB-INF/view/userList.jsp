<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/1
  Time: 23:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
    <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="index.html">首页</a></li>
                <li><a href="/" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a >管理员：${sessionScope.get('user').name}，欢迎使用！</a></li>
               <%-- <li><a href="#">修改密码</a></li>--%>
                <li><a href="${pageContext.request.contextPath}/cancellation">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/userList"><i class="icon-font">&#xe006;</i>用户列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="${pageContext.request.contextPath}/jumpSuccess">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">用户列表</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
              <%--  <form action="#" method="post">--%>
                    <table class="search-tab">
                        <tr>
                            <%--  <th width="120">选择分类:</th>
                              <td>
                                  <select name="search-sort" id="">
                                      <option value="">全部</option>
                                      <option value="19">精品界面</option><option value="20">推荐界面</option>
                                  </select>
                              </td>--%>
                            <th width="100">昵称/手机号:</th>
                            <td><input class="common-text" placeholder="昵称/手机号" name="keywords" value="" id="userName" type="text"></td>
                            <td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit" onclick="queryUser()"></td>
                                <td>&nbsp;</td>
                                <td><a href="${pageContext.request.contextPath}/exportExcel"><input class="btn btn-primary btn2" name="sub" value="导出Excel表格" type="submit" ></a></td>
                        </tr>
                    </table>
              <%--  </form>--%>
            </div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <%--   <div class="result-title">
                       <div class="result-list">
                           <a href="insert.html"><i class="icon-font"></i>新增作品</a>
                           <a id="batchDel" href="javascript:void(0)"><i class="icon-font"></i>批量删除</a>
                           <a id="updateOrd" href="javascript:void(0)"><i class="icon-font"></i>更新排序</a>
                       </div>
                   </div>--%>
                <div class="result-content" id="table">
                    <table class="result-tab" width="100%" id="Article">
                        <tr>
                            <th>昵称</th>
                            <th>手机号</th>
                            <th>邮箱</th>
                            <th>登录时间</th>
                            <th>退出时间</th>
                            <th>注册时间</th>
                            <th>登录频次</th>
                            <th>等级</th>
                        </tr>
                        <c:forEach var="user" items="${requestScope.get('users')}">
                            <tr >
                                <td> ${user.getName()} </td>
                                <td>${user.getPhonenumber()}</td>
                                <td title="">${user.getMailbox()}
                                </td>
                                <td>${user.getLogintime()}</td>
                                <td> ${user.getOuttime()}</td>
                                <td>${user.getRegistertime()}</td>
                                <td>${user.getFrequency()}</td>
                                <td>
                                   ${user.getLevel()}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <div class="list-page">
                        <a onclick="nextPage('last')">上一页</a>
                        <span id="num2">${userSum.getUserSum()}</span>条
                        <span id="currentPage">1</span>/
                      <c:if test="${userSum.getUserSum()%5>0}">
                            <fmt:parseNumber var="number" integerOnly="true" value="${userSum.getUserSum()/5+1}"  />
                            <span id="num"> <fmt:parseNumber  integerOnly="true" value="${userSum.getUserSum()/5+1}"  /></span>
                        </c:if>
                        <c:if test="${userSum.getUserSum()%10<=0}">
                            <fmt:parseNumber var="number"   integerOnly="true" value="${userSum.getUserSum()/5}"  />
                            <span id="num"> <fmt:parseNumber  integerOnly="true" value="${userSum.getUserSum()/5}"  /></span>
                        </c:if>页

                        <c:forEach var="i" begin="1" end="${number}">
                            <c:if test="${i<=25}">
                                <a onclick="nextPage(${i})"> ${i}</a>
                            </c:if>
                        </c:forEach>
                        <a onclick="nextPage('next')">下一页</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
<script>
    var i = 1;//页数
    function nextPage(n) {//查询分页
        if (n == "next") {
            i++;

        } else if (n == "last") {
            i--;

        } else {
            i = n;
        }
        if (i > parseInt($("#num").text())) {
            i = parseInt($("#num").text());
            alert("没有数据了")
            return;
        }
        if (i < 1) {
            i = 1;
            alert("没有数据了")
            return

        }
        $("#Article").remove();
        $("#currentPage").text(i);
        //  alert("text" + i);
        $.post("${pageContext.request.contextPath}/updateUserList",
            {
                n: i,
            },
            function (data) {
                var json = JSON.parse(data);
                var txt1 = "<table class=\"result-tab\" width=\"100%\" id=\"Article\"> <tr> <th>昵称</th> <th>手机号</th> <th>邮箱</th> <th>登录时间</th> <th>退出时间</th> <th>注册时间</th> <th>登录频次</th> <th>等级</th> </tr> ";
                var txt3 = "</table>";
                $("#table").prepend(txt1 + txt3);
                for (i2 in json) {
                    var txt2 = "<tr id=\"" +json[i2].id+ "\"> <td>" + json[i2].name + "</td> <td >" + json[i2].phonenumber + " </td> <td>" + json[i2].mailbox + "</td> <td>"+json[i2].logintime+"</td> <td>" + json[i2].outtime + "</td> <td>" + json[i2].registertime + "</td> <td>" + json[i2].frequency + "</td> <td> "+json[i2].level+"  </td> </tr> ";
                    $("#Article").append(txt2);
                }


            });
    }

    function queryUser(){//查询用户

        $("#Article").remove();
            $.post("${pageContext.request.contextPath}/queryUser",
                {
                   name:$("#userName").val()
                },
                function(data){
                    var json = JSON.parse(data);
                    var txt1 = "<table class=\"result-tab\" width=\"100%\" id=\"Article\"> <tr> <th>昵称</th> <th>手机号</th> <th>邮箱</th> <th>登录时间</th> <th>退出时间</th> <th>注册时间</th> <th>登录频次</th> <th>等级</th> </tr> ";
                    var txt3 = "</table>";
                    $("#table").prepend(txt1 + txt3);
                    for (i2 in json) {
                        var txt2 = "<tr id=\"" +json[i2].id+ "\"> <td>" + json[i2].name + "</td> <td >" + json[i2].phonenumber + " </td> <td>" + json[i2].mailbox + "</td> <td>"+json[i2].logintime+"</td> <td>" + json[i2].outtime + "</td> <td>" + json[i2].registertime + "</td> <td>" + json[i2].frequency + "</td> <td> "+json[i2].level+"  </td> </tr> ";
                        $("#Article").append(txt2);
                    }

                });

    }


</script>
</body>
</html>