﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>博客网站后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="${pageContext.request.contextPath}/jumpSuccess">首页</a></li>
                <li><a href="http://wanwan.group/" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">

                <li><a >管理员：${sessionScope.get('user').name}，欢迎使用！</a></li>
             <%--   <li><a href="#">修改密码</a></li>--%>
                <li><a href="${pageContext.request.contextPath}/cancellation">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/userList"><i class="icon-font">&#xe006;</i>用户列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>

                    </ul>
            <ul>
            </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font">&#xe06b;</i><span>欢迎使用</span></div>
        </div>
     <%--   <div class="result-wrap">
            <div class="result-title">
                <h1>快捷操作</h1>
            </div>
            <div class="result-content">
                <div class="short-wrap">
                    <a href="${pageContext.request.contextPath}/insert"><i class="icon-font">&#xe001;</i>新增作品</a>
                    <a href="${pageContext.request.contextPath}/insert"><i class="icon-font">&#xe005;</i>新增博文</a>
                    <a href="${pageContext.request.contextPath}/insert"><i class="icon-font">&#xe048;</i>新增作品分类</a>
                    <a href="${pageContext.request.contextPath}/insert"><i class="icon-font">&#xe041;</i>新增博客分类</a>
                    <a href="#"><i class="icon-font">&#xe01e;</i>作品评论</a>
                </div>
            </div>
        </div>--%>
        <div class="result-wrap">
            <div class="result-title">
                <h1>系统基本信息</h1>
            </div>
            <div class="result-content">
                <ul class="sys-info-list">
                    <li>
                        <label class="res-lab">管理员数量：</label><span class="res-info">${list.getAdministratorNumber()}</span>
                    </li>
                    <li>
                        <label class="res-lab">用户数量：</label><span class="res-info">${list.getUserNumber()}</span>
                    </li>
                    <li>
                        <label class="res-lab">网站访问数量：</label><span class="res-info">${list.getWebsiteNumber()}</span>
                    </li>
                    <li>
                        <label class="res-lab">网站文章数量：</label><span class="res-info">${list.getArticleNumber()}</span>
                    </li>

                    <li>
                        <label class="res-lab">运行环境</label><span class="res-info">Apache/tomcat 9.0</span>
                    </li>
                    <li>
                        <label class="res-lab">当前登录时间</label><span class="res-info">${time}</span>
                    </li>
                    <li>
                        <label class="res-lab">服务器域名/IP</label><span class="res-info">wanwan.group</span>
                    </li>

                </ul>
            </div>
        </div>
      <%--  <div class="result-wrap">
            <div class="result-title">
                <h1>使用帮助</h1>
            </div>
            <div class="result-content">
                <ul class="sys-info-list">
                    <li>
                        
                    </li>
                </ul>
            </div>
        </div>--%>
    </div>
    <!--/main-->
</div>
<style>
.copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>
<div class="copyrights" id="links20210126">
	Collect from <a href="http://www.cssmoban.com/"  title="网站模板">模板之家</a>
	<a href="https://www.chazidian.com/"  title="查字典">查字典</a>
</div>
</body>
</html>