<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/1
  Time: 16:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 默认的媒体对象</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <h2>${previewSiteArticles.getTitle()} &nbsp;&nbsp;<small>时间：${previewSiteArticles.getTime()}
    </small>&nbsp;&nbsp;<small>作者：${previewSiteArticles.getName()}</small>&nbsp;&nbsp;</h2>
   <input type="hidden" value=" ${previewSiteArticles.getTitle()}"  id="title">
    <div class="media">
        <div class="media-left">
            <img src="${previewSiteArticles.getPicture()} " class="media-object" style="width:300px;height: 200px">
        </div>
        <div class="media-body">
         <h1><p>  ${previewSiteArticles.getContent()}</p></h1>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.title=$("#title").val()
</script>
</body>
</html>