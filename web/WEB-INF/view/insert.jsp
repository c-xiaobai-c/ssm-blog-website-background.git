<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
    <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="index.html">首页</a></li>
                <li><a href="#" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a href="http://www.dede58.com">管理员</a></li>
                <li><a href="http://www.dede58.com">修改密码</a></li>
                <li><a href="http://www.dede58.com">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/userList"><i class="icon-font">&#xe006;</i>用户列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="${pageContext.request.contextPath}/jumpSuccess">首页</a><span class="crumb-step">&gt;</span><a class="crumb-name" href="${pageContext.request.contextPath}/siteArticles">站点文章</a><span class="crumb-step">&gt;</span><span>新增作品</span></div>
        </div>
        <div class="result-wrap">
            <div class="result-content">
           <%--     <form action="/jscss/admin/design/add" method="post" id="myform" name="myform" enctype="multipart/form-data">--%>
                    <table class="insert-tab" width="100%">
                        <tbody>
                       <%-- <tr>
                            <th width="120"><i class="require-red">*</i>分类：</th>
                            <td>
                                <select name="colId" id="catid" class="required">
                                    <option value="">请选择</option>
                                    <option value="19">精品界面</option><option value="20">推荐界面</option>
                                </select>
                            </td>
                        </tr>--%>
                            <tr>
                                <th><i class="require-red">*</i>标题：</th>
                                <td>
                                    <input class="common-text required" id="title" name="title" size="50" value="" type="text">
                                </td>
                            </tr>
                            <tr style="display: none">
                                <th>作者：</th>
                                <td><input class="common-text" name="author" id="name" size="50" value="${sessionScope.get('user').name}" type="text" ></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>上传图片：</th>
                                <td><input name="smallimg" id="image" type="file"><!--<input type="submit" onclick="submitForm('/jscss/admin/design/upload')" value="上传图片"/>--></td>
                            </tr>
                           <tr>
                               <th><i class="require-red">*</i>缩略图：</th>
                           <td><img id="showImg" width="50px" height="50px"></td>
                           </tr>
                            <tr>
                                <th>内容：</th>
                                <td><textarea name="content" class="common-textarea" id="content" cols="30" style="width: 98%;" rows="10"></textarea></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input class="btn btn-primary btn6 mr10" value="提交" type="submit"  onclick="save()">
                                    <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                                </td>
                            </tr>
                        </tbody></table>
              <%--  </form>--%>
            </div>
        </div>

    </div>
    <!--/main-->
</div>


<script type="text/javascript">

    var  base64Data;
    //给file添加一个事件,当选择了文件后,执行下面uploadFile的方法
    $(function () {
        $("#image").bind("change", function() {
            uploadFile($(this));
        });
    })

    //通过onChange直接获取base64数据
    function uploadFile(f) {
        var reader = new FileReader();
        var allowImgFileSize = 2100000; //上传图片最大值(单位字节)（ 2 M = 2097152 B ）超过2M上传失败
        var file = f[0].files[0];
        if (file) {
            //将文件以DataURL形式读取
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                if (allowImgFileSize != 0 && allowImgFileSize < reader.result.length) {
                    alert( '上传失败，请上传不大于2M的图片！');
                    return;
                }else{
                    $("#showImg").attr("src",reader.result);
                    console.log(reader.result);
                    base64Data=reader.result;
                  //  ajaxUpload(reader.result);
                }
            }
        }
    }

    function save(){
      if($("#title").val()!=""&&$("#content").val()!=""&&$("#name").val()!=null) {
      //  alert($("#content").val());
    $.post("${pageContext.request.contextPath}/saveSiteArticles",
        {
            name:$("#name").val(),
            title:$("#title").val(),
            picture: base64Data,
            content:$("#content").val()
        },
        function (data) {
        if(data=="success"){
            window.location.assign("${pageContext.request.contextPath}/siteArticles")
        }
        else{
            alert(data);
        }

        });
      }
      else {
          alert("不能为空")
      }
    }



 /*   //ajax异步上传
    function ajaxUpload(base64Data) {
        var url = "${path}/uploadBase64";
        $.ajax({
            url: url,
            type: "post",
            dataType: "text",
            data: {
                base64Data : base64Data
            },
            success: function(data){
                console.log(data);
                //上传成功后将图片展示出来（相当于回显）
                $("#showImg").attr("src",base64Data);
            }
        });
    }*/
</script>


</body>
</html>