<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/1
  Time: 16:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 默认的媒体对象</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>${previewArticle.getTitle()} &nbsp;&nbsp;<small>时间：${previewArticle.getTime()}
    </small>&nbsp;&nbsp;<small>作者：${previewArticle.getName()}</small>&nbsp;&nbsp;<small style="color: red">状态：${previewArticle.getStatus()}</small></h2>
   <input type="hidden" value=" ${previewArticle.getTitle()}"  id="title">
    <div class="media">
        <div class="media-body">
            <p>  ${previewArticle.getContent()}</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.title=$("#title").val()
</script>
</body>
</html>