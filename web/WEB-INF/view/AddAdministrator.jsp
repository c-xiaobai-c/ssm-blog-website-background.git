<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/5
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
    <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>

    <script>
        function validateForm(){
          //  history.go(-1)
            if ($("#name").val() == "") {
                alert("账号不能为空");
            }
           else if ($("#account").val() == "") {
                alert("账号不能为空");
            }
            else if ($("#password").val() == "") {
                alert("密码不能为空");
            }
           else if ($("#password2").val() == ""){
                alert("密码不能为空");
            }
            else if($("#password2").val()!=$("#password").val()){
                alert("两次密码不一样，请重新输入");
            }
            else{
                $.post("${pageContext.request.contextPath}/add",
                    {
                        name:$("#name").val(),
                        account:$("#account").val(),
                        password:$("#password").val(),
                        password2:$("#password2").val()
                    },
                    function(data){
                         alert(data);
                       // history.go(-1)
                        window.location.assign("${pageContext.request.contextPath}/AdministratorList")

                    });

            }

        }


    </script>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="index.html">首页</a></li>
                <li><a href="#" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a >管理员：${sessionScope.get('user').name}，欢迎使用！</a></li>
             <%--   <li><a href="">修改密码</a></li>--%>
                <li><a href="${pageContext.request.contextPath}/cancellation">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe006;</i>分类管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="${pageContext.request.contextPath}/jumpSuccess">首页</a><span class="crumb-step">&gt;</span><a class="crumb-name" href="${pageContext.request.contextPath}/AdministratorList/">管理员列表</a><span class="crumb-step">&gt;</span><span>新增管理员</span></div>
        </div>
        <div class="result-wrap">
            <div class="result-content">
              <%--  <form  name="myForm" onsubmit="return validateForm()" action="${pageContext.request.contextPath}/add" method="post" id="myform" >--%>
                    <table class="insert-tab" width="100%">
                        <tbody>

                        <tr>
                            <th><i class="require-red">*</i>管理员名称：</th>
                            <td>
                                <input class="common-text required" id="name" name="name" size="50" value="" type="text" required>
                            </td>
                        </tr>
                        <tr>
                            <th>账号：</th>
                            <td><input class="common-text" name="account" id="account" size="50" value="admin" type="number" placeholder="手机号" required></td>
                        </tr>
                        <tr>
                            <th><i class="require-red">*</i>密码：</th>
                            <td>
                                <input class="common-text required" id="password" name="password" size="50" value="" type="password"  placeholder="密码(必须包括字母和数字,字符长度大于等于8)" required>
                            </td>                        </tr>
                        <tr>
                            <th><i class="require-red">*</i>确认密码：</th>
                            <td>
                                <input class="common-text required" id="password2" name="password2" size="50" value="" type="password" required>
                            </td>                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input class="btn btn-primary btn6 mr10" value="提交" onclick="validateForm()"  type="submit">
                                <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                            </td>
                        </tr>
                        </tbody></table>
<%--
                </form>
--%>
            </div>
        </div>

    </div>
    <!--/main-->
</div>
</body>
</html>