<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/6/3
  Time: 8:08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="order by dede58.com"/>
    <title>后台管理</title>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="..${pageContext.request.contextPath}/statics/css/main.css"/>
    <script src="..${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="index.html">首页</a></li>
                <li><a href="/" target="_blank">网站首页</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a >管理员：${sessionScope.get('user').name}，欢迎使用！</a></li>
             <%--   <li><a href="#">修改密码</a></li>--%>
                <li><a href="${pageContext.request.contextPath}/cancellation">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                <li>
                    <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                    <ul class="sub-menu">
                        <li><a href="${pageContext.request.contextPath}/design"><i class="icon-font">&#xe005;</i>博文管理</a></li>
                        <li><a href="${pageContext.request.contextPath}/userList"><i class="icon-font">&#xe006;</i>用户列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/accessList"><i class="icon-font">&#xe004;</i>访问列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/websiteDataAnalysis"><i class="icon-font">&#xe012;</i>数据分析</a></li>
                        <li><a href="${pageContext.request.contextPath}/AdministratorList"><i class="icon-font">&#xe052;</i>管理员列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/siteArticles"><i class="icon-font">&#xe052;</i>站点文章</a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                        <li><a></a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="${pageContext.request.contextPath}/jumpSuccess">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">访问列表</span></div>
        </div>

        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-content" id="table">
                    <table class="result-tab" width="100%" id="Article">
                        <tr>
                            <th>序号</th>
                            <th>访问时间</th>
                            <th>访问地址</th>

                        </tr>
                        <c:forEach var="accessList" items="${requestScope.get('accessLists')}">
                            <tr >
                                <td> ${accessList.getId()} </td>
                                <td>${accessList.getTime()}</td>
                                <td title="">${accessList.getAddress()}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    <div class="list-page">
                        <a onclick="nextPage('last')">上一页</a>
                        <span id="num2">${accessList.getAccessListSum()}</span>条
                        <span id="currentPage">1</span>/
                        <c:if test="${accessList.getAccessListSum()%10>0}">
                            <fmt:parseNumber var="number" integerOnly="true" value="${accessList.getAccessListSum()/10+1}"  />
                            <span id="num"> <fmt:parseNumber  integerOnly="true" value="${accessList.getAccessListSum()/10+1}"  /></span>
                        </c:if>
                        <c:if test="${accessList.accessListSum%10<=0}">
                            <fmt:parseNumber var="number"   integerOnly="true" value="${accessList.getAccessListSum()/10}"  />
                            <span id="num"> <fmt:parseNumber  integerOnly="true" value="${accessList.getAccessListSum()/10}"  /></span>
                        </c:if>页

                        <c:forEach var="i" begin="1" end="${number}">
                            <c:if test="${i<=25}">
                                <a onclick="nextPage(${i})"> ${i}</a>
                            </c:if>
                        </c:forEach>
                        <a onclick="nextPage('next')">下一页</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
<script>
    var i = 1;//页数
    function nextPage(n) {//查询分页
        if (n == "next") {
            i++;

        } else if (n == "last") {
            i--;

        } else {
            i = n;
        }
        if (i > parseInt($("#num").text())) {
            i = parseInt($("#num").text());
            alert("没有数据了")
            return;
        }
        if (i < 1) {
            i = 1;
            alert("没有数据了")
            return

        }
        $("#Article").remove();
        $("#currentPage").text(i);
        //  alert("text" + i);
        $.post("${pageContext.request.contextPath}/updateAccessList",
            {
                n: i,
            },
            function (data) {
                var json = JSON.parse(data);
                var txt1 = "<table class=\"result-tab\" width=\"100%\" id=\"Article\"> <tr>  <th>序号</th><th>访问时间</th><th>访问地址</th></tr>";
                var txt3 = "</table>";
                $("#table").prepend(txt1 + txt3);
                for (i2 in json) {
                    var txt2 = "<tr id=\"" +json[i2].id+ "\"> <td>" + json[i2].id + "</td> <td >" + json[i2].time + " </td> <td>" + json[i2].address + "</td>  </tr> ";
                    $("#Article").append(txt2);
                }


            });
    }


</script>
</body>
</html>